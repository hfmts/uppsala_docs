# Dokumente

Als Dokumente werden in Uppsala die PDFs bezeichnet, welche erstellt werden können.

### Dokumente generieren

#### Zeugnis generieren

Das Zeugnis wird auf dem Eintrag *[Zeugnis](laufende_daten.md#zeugnis)* erstellt. Siehe auch das [Kapitel Laufende Daten](laufende_daten.md#zeugnis).

  [![Zeugnis](/docs/tmb_grade_report.png)](/docs/ENR-000247_Zeugnis.pdf)

Nachfolgend eine Anleitung zur Erstellung eines Zeugnis.

[![Zeugnis erstellen](images/grade_report_1.gif)](/images/grade_report_1.gif)

#### Studienbestätigung generieren

Die Studienbestätigung wird auf dem Eintrag *[Studienbestätigung](laufende_daten.md#studienbestätigung)* erstellt.

[![Studienbestätigung](/docs/tmb_enrollment_confirmation.png)](/docs/ENR-000247_Studienbestätigung.pdf)

Nachfolgend eine Anleitung zur Erstellung einer Studienbestätigung.

[![Studienbestätigung erstellen](images/enrollment_confirmation_1.gif)](images/enrollment_confirmation_1.gif)

#### Studentenrechnung generieren

Die Studentenrechnung wird auf dem Eintrag *[Studentenrechnung](#studentenrechnung)* erstellt.

[![Studentenrechnung](/docs/tmb_bill.png)](/docs/BILL-000001.pdf)

Nachfolgend eine Anleitung zur Erstellung einer Studentenrechnung.
[![Studentenrechnung erstellen](images/print_student_bill_1.gif)](images/print_student_bill_1.gif)

### Mehrere Dokumente gleichzeitig generieren

Es lassen sich auch mehrere Dokumente gleichzeitig generieren. Nachfolgend eine Anleitung zur Erstellung von mehreren Zeugnissen.

[![Mehrere Zeugnisse erstellen](images/bulk_generation_1.gif)](images/bulk_generation_1.gif)


