# Neue Klasse

Eine neue Schulklasse lässt sich mit folgenden Schritten anlegen:

1. Einen neue Klasse in der Tabelle *[Schulklasse](stammdaten.md#schulklasse)* erstellen.
2. Alle neuen Studierenden in der Tabelle *[StudentIn](stammdaten.md#studentin)* anlegen.
3. Die *[Kursdurchführungen](laufende_daten.md#kursdurchführung)* aus der letzten relevanten Zeitspanne neu durchführen. Dazu bietet sich die Aktion *Erneut durchführen* an. Sollten *[Kurse](stammdaten.md#kurs)* angeboten werden, die in der letzten relevanten Zeitspanne nicht durchgeführt wurden, diese entsprechend manuell ergänzen.
4. Für jede neue *[StudentIn](stammdaten.md#studentin)* eine neue *[Einschreibung](laufende_daten.md#einschreibung)* erstellen. Dabei die Studierenden in die [Kurse einschreiben](laufende_daten.md#kurseinschreibungen).


