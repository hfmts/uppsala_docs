# Listen erstellen und exportieren

Uppsala generiert einige Listen zur besseren Übersicht des Schulalltags.

### Adressliste

[![Adressliste](/images/reports_address_list_1.gif)](/images/reports_address_list_1.gif)


### Notenliste

[![Notenliste](/images/reports_grade_list_1.gif)](/images/reports_grade_list_1.gif)


