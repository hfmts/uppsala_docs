# Laufende Daten

Unter laufenden Daten werden alle jene Daten in Uppsala verstanden, die während einer *Zeitspanne* erfasst werden, zB. alle Prüfungsresultate eines Semesters.

## Kursdurchführung

Die *Kursdurchführung* bildet den eigentlichen Unterricht eines Kurses ab und verknüpft einen *Kurs* mit einer *Zeitspanne* und einer *Stufe*.

```admonish help title="Beispiel"
Der *Kurs* `Chemie` wird in der *Zeitspanne* `Sommersemester 2022` auf der *Stufe* `Semester 3` durchgeführt.
```

### Kursdurchführung erneut durchführen

*Kursdurchführungen* können bequem erneut durchgeführt werden. Dabei wird eine Kopie der ausgewählten *Kursdurchführung* erzeugt und eine neue *Zeitspanne* verwendet.


[![Kursdurchführung erneut durchführen](images/course_execution_reexecute_bulk.gif)](images/course_execution_reexecute_bulk.gif)


## Einschreibung

Die *Einschreibung* verknüpft eine [*StudentIn*](stammdaten.md#studentin) mit einer [*Zeitspanne*](stammdaten.md#zeitspanne), einer [*Stufe*](stammdaten.md#stufe), einer [*Schulklasse*](stammdaten.md#schulklasse) und einer Liste von [*Kursdurchführungen*](stammdaten.md#kursdurchführung). 

```admonish help title="Beispiel"
Der [*Student*](stammdaten.md#studentin) `Max Romeo` ist im `Herbstsemester 2023` ([*Zeitspanne*](stammdaten.md#zeitspanne)) im `Semester 3` ([*Stufe*](stammdaten.md#stufe)) und der [*Schulklasse*](stammdaten.md#schulklasse) `Klasse Frühling 21` eingeschrieben.
```

Die Tabelle *Einschreibung* ist die Basis für einige grundlegende Konzepte von Uppsala:

1. Jede [*StudentIn*](stammdaten.md#studentin) wird pro [*Zeitspanne*](stammdaten.md#zeitspanne) einmal eingeschrieben.
2. Zeugnisse, Studienbestätigungen und Rechnungen werden pro *Einschreibung* ausgestellt.
3. Eine typische schulische Laufbahn eines Studierenden hat für jede Stufe einer Ausbildung eine *Einschreibung*.

```admonish help title="Beispiel"
Die *[Studentin](stammdaten.md#studentin)* `Jill Scott` besucht eine Ausbildung zur Gemüsegärtnerin, welche drei Sommer dauert. . Jeden Sommer findet ein Modul statt. Am Ende der Ausbildung hat `Jill Scott` folgende Einträge in der Tabelle *[Einschreibung](laufende_daten.md#einschreibung)*:

|#|StudentIn|Stufe|Zeitspanne|Schulklasse|
|-|-|-|-|-|
|1|Jill Scott|Modul 1|Sommer 2020|Jahrgang 2020|
|2|Jill Scott|Modul 2|Sommer 2021|Jahrgang 2020|
|3|Jill Scott|Modul 3|Sommer 2022|Jahrgang 2020|

Für jedes Modul wird einzeln eine Rechnung sowie ein Zeugnis ausgestellt.
```

### Einschreibung erneuern (Aufsteigen lassen)

Eine *Einschreibung* lässt sich gewissermassen duplizieren und einfach anpassen. Diese Funktionalität erlaubt die einfache Erstellung von Einschreibungen zB. einer ganzen Klasse zum Beginn eines neuen Semesters basierend auf den Einschreibungen des alten Semesters. Die Studierenden steigen auf, dh. sie werden für die kommende *[Zeitspanne](stammdaten.md#zeitspanne)* (zB. `Wintersemester 22/23`) in die nächste *[Stufe](#stufe)* (zB. `5.tes Semester`) eingeschrieben.

Dabei werden grundsätzlich alle Daten aus der bestehenden *Einschreibung* übernommen und zusätzlich folgende Felder der neuen *Einschreibung* neu gesetzt:

* *Einschreibung ➔ Stufe*: Nächste *[Stufe](stammdaten.md#stufe)* in der Kette von *Stufen*
* *Einschreibung ➔ Zeitspanne*: Nächste *[Zeitspanne](#zeitspanne)* in der Kette von *Zeitspannen*
* *Einschreibung ➔ Kurseinschreibungen*: Falls *Kursdurchführungen automatisch laden* aktiviert ist, so wird der Studierende in alle Kursdurchführungen dieser Stufe und Zeitspanne eingeschrieben.

```admonish warning title="Achtung"
Kursdurchführungen müssen für die neue Zeitspanne und Stufe bereits existieren, damit die Kursdurchführungen automatisch geladen werden können.
```

[![Aufsteigen lassen](images/enrollment_level_up_1.gif)](images/enrollment_level_up_1.gif)


## Kurseinschreibungen

Innerhalb einer Semestereinschreibung (*[Einschreibung](#einschreibung)*) können mehrere Kurse besucht werden. Jede *[Studentin](#studentin)* wird also jedes Semster in mehrere *[Kursdurchführungen](laufende_daten.md#kursdurchführung)* eingeschrieben. Die Studierenden einer Klasse müssen nicht alle die gleichen *Kursdurchführungen* besuchen. Diese Flexibilität erlaubt zB. die Abbildung von Niveaukursen (zB. `Englisch A2` und `Englisch A1`) oder die Suspendierung einer einzelenen *Studentin* von einem Kurs.

```admonish help title="Beispiel"
Die *[Studentin](#studentin)* `Jill Scott` aus dem vorhergehenden Beispiel ist in der *[Einschreibung](#einschreibung)* #1 (`Modul 1`, `Sommer 2020`, `Jahrgang 2020`) in folgende *[Kursdurchführungen](laufende_daten.md#kursdurchführung)* eingeschrieben:

|#|Kursdurchführung|
|-|-|
|1|Pflanzenkunde 1|
|2|Boden-Luft Prozesse|
|3|Biologische Schädlingsbekämpfung|
```

Kurseinschreibungen können innerhalb der *[Einschreibung](#einschreibung)* bearbeitet werden. 

[![Kurseinschreibungen bearbeiten](images/course_enrollments.png)](images/course_enrollments.png)

### Serien-Kurseinschreibungen ersetzen

Kurseinschreibungen können auch für eine Serie von Einschreibungen gleichzeitig erstellt werden. Beim Ersetzen der Kurseinschreibungen werden alle bisherigen Kurseinschreibungen gelöscht und die neuen Kurseinschreibungen erstellt. 

[![Serien-Kurseinschreibungen](images/course_enrollment_replace.gif)](images/course_enrollment_replace.gif)

### Serien-Kurseinschreibungen hinzufügen

Kurseinschreibungen können auch für eine Serie von Einschreibungen gleichzeitig erstellt werden. Beim Hinzufügen von Kurseinschreibungen werden alle bisherigen Kurseinschreibungen belassen und die neuen Kurseinschreibungen zu den Bisherigen hinzugefügt. 

[![Serien-Kurseinschreibungen](images/course_enrollment_append.gif)](images/course_enrollment_append.gif)

## Beurteilungen

Jede *[Kursdurchführung](laufende_daten.md#kursdurchführung)* kann beliebig viele Beurteilungen enthalten. Eine Beurteilung ist zB. die Note einer Prüfung oder schriftlichen Arbeit. Dabei wird zwischen zwei Beurteilungstypen unterschieden: *Nummer* und *Text*. 

Eine *Beurteilung* vom Typ *Nummer* wird verwendet um Noten einzutragen. Für die Erstellung des Zeugnisses wird der Durchschnitt unter Berücksichtigung der Gewichtung automatisch berechnet.

Eine *Beurteilung* vom Typ *Text* wird verwendet, um zB. ein Fach ohne Leistungsnachweis festzuhalten. Die Beurteilung beinhaltet dann zB. die Beurteilung `besucht` oder `nicht besucht`. 

Eine *[Kursdurchführung](laufende_daten.md#kursdurchführung)* sollte nur *Beurteilungen* vom Typ *Nummer* oder eine einzige *Beurteilung* vom Typ *Text* enthalten.


[![Beurteilung erstellen](images/assessment_new_1.gif)](images/assessment_new_1.gif)

## Anwesenheiten

Jede *[Kursdurchführung](laufende_daten.md#kursdurchführung)* kanne eine *Anwesenheit* enthalten. Die *Anwesenheit* erfasst ob die *StudentInnnen* einen Kurs besucht haben. Die *Anwesenheit* wird im Zeugnis ausgewiesen. 


[![Anwesenheit erstellen](images/attendance_new_1.gif)](images/attendance_new_1.gif)

## Studentenrechnung

*Studentenrechnungen* erlauben die Erstellung von [Studentenrechnungsdokumenten](#studentenrechnung-generieren). Studentenrechnungen richten sich an *StudentInnen* und verrechnen grundsätzlich die bezogenen Leistungen einer *[Einschreibung](#einschreibung)*.

Bei der Erstellung einer *Studentenrechnung* werden folgende Automatismen ausgelöst:

1. Mittels der verknüpften *[Einschreibung](#einschreibung)* wird die *[Stufe](#stufe)* und *[Zeitspanne](#zeitspanne)* bestimmt und als Position in der Rechnung aufgelistet
2. Anhand der *[Stufe](#stufe)* werden die Standardgebühren der Einschreibung ermittelt
3. Das Rechnungsdatum wird auf das aktuelle Datum gesetzt. Dieses Datum kann nachträglich geändert werden.
4. Die Zahlungsfrist wird auf einen Monat nach dem Rechnungsdatum gesetzt. Wird das Rechnungsdatum manuell angepasst, so wird die Zahlungsfrist entsprechend nachgeführt.
5. Für die Rechnung wird eine neue 27-stellige QR-Referenznummer erstellt.
6. Der Rechungsstatus wird auf *Offen* gesetzt. Sobald die Rechnung beglichen wird, kann der Status auf *Geschlossen* gesetzt werden.
7. Der Rechnungstyp wird auf *Rechnung* gesetzt. Zur Erstellung einer ersten oder zweiten Mahnung kann der Rechnungstyp entsprechend angepasst werden.

[![Studentenrechnung erstellen](images/student_bill_new_1.gif)](images/student_bill_new_1.gif)

### Studentenrechnungen via Einschreibungen erstellen

Studentenrechnungen lassen sich auch aus Einschreibungen erstellen. Dies erlaubt eine Serienbearbeitung mehrerer Einschreibungen.

[![Studentenrechnung erstellen](images/student_bill_new_2.gif)](images/student_bill_new_2.gif)

## Zeugnis

Das *Zeugnis* verweist immer auf eine [*Einschreibung*](laufende_daten.md#Einschreibung) und wird verwendet um anschliessend das Zeugnis als Dokument abzuspeichern. Zeugnisse lassen sich wie [die Studentenrechnungen via Einschreibungen erstellen](laufende_daten.md#studentenrechnungen-via-einschreibungen-erstellen). Weitere Informationen sind auch im [Kapitel Dokumente](dokumente.md) zu finden.


[![Verweise auf die Einschreibung](images/enrollment_links.png)](images/enrollment_links.png)

## Studienbestätigung

Die *Studienbestätigung* verweist immer auf eine [*Einschreibung*](laufende_daten.md#Einschreibung) und wird verwendet um anschliessend eine Studienbestätigung als Dokument abzuspeichern. Weitere Informationen sind auch im [Kapitel Dokumente](dokumente.md) zu finden.
