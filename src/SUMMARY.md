# Schuladministration mit Uppsala

Uppsala ist eine Schulverwaltungssoftware mit Fokus auf Höhere Fachschulen in der Schweiz. Die Konzepte sollten sich allerdings ebenfalls auf Seminar, Universitäten und Volksschulen anwenden lassen.

- [Einleitung](./einleitung.md)
- [Ziele](./ziele.md)
- [Stammdaten](./stammdaten.md)
- [Laufende Daten](./laufende_daten.md)
- [Einstellungen](./einstellungen.md)
- [Neue Klasse](./neue_klasse.md)
- [Neues Semester](./neues_semester.md)
- [Listen erstellen und exportieren](./listen.md)
- [Dokumente](./dokumente.md)
- [Datenbankmodel](./datenbankmodel.md)
- [Changelog](./changelog.md)

