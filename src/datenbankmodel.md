# Datenbankmodel

Das Datenbankmodel zeigt alle in Uppsala verwendeten Tabellen, deren Felder und die Abhängigkeiten zwischen den Feldern.

[aktuelles Datenbankmodel als PDF](https://gitlab.com/hfmts/uppsala/-/raw/main/doc/imgs/database_scheme.latest.pdf)
![aktuelles Datenbankmodel](https://gitlab.com/hfmts/uppsala/-/raw/main/doc/imgs/database_scheme_p1.latest.svg)
![aktuelles Datenbankmodel](https://gitlab.com/hfmts/uppsala/-/raw/main/doc/imgs/database_scheme_p2.latest.svg)
