# Changelog

## Version 1.6

### 1.6.0

* Feature: *Term (Semester)* erscheinen in Verknüpfungen und Filterauswahlen nun in der Reihe der Ordnungszahl (Ordinal Number). Siehe [Zeitspanne](./stammdaten.html#zeitspanne).
* Bugfix: Farbige Semester landen wieder im Filter wenn angeklickt.
* Feature: Farbige Semester nun auch in *Course Execution*.
* Bugfix: *Create Grade Report* richtig übersetzt in *Zeugnis erstellen*

## Version 1.5

### 1.5.1

* Serverfehler beim Öffnen von Uppsala behoben

### 1.5.0

* [Serien-Kurseinschreibungen](./laufende_daten.html#serien-kurseinschreibungen-ersetzen) zur einfachen Einschreibung von Studierenden in Kursdurchführungen
* Stundenplanverarbeitung mit besserer Fehlerbeschreibung bei Formfehler des Excels
* Stundenplanverarbeitung sollte nun nicht mehr immer den alten Durchlauf anzeigen
* Stundenplanverarbeitung Anwesenheitslisten verbessert


## Version 1.2

### 1.2.0

* *Rechnung Student (Student Bill)* hat neu ein Feld `total_description` und `total_amount` zur Speicherung des gesamten Rechnungsbetrags.
* Skript zur Migration der alten Einträge *Rechnung Student* auf neue Felder `total_description` und `total_amount`.
* Neue Liste *Debitorenliste (Debtor List)* zieht alle Forderungen aus *Rechnung Student* und *Generische Rechnung* zusammen.
* *StudentIn* hat ein zusätzliches Feld für einen Adresszusatz in der seperaten Rechnungsadresse. Der Adresszusatz wird in der Studentenrechnung in der Adresse des Briefs angezeigt, aber nicht auf der QR-Rechnung.
* *Stufe (Level)* wird in Liste `Einschreibung (Enrollment)` farbig angezeigt:
![Stufe farbig](./images/enrollement_level_color.png)

## Version 1.1

### 1.1.0

* Funktion `PDF abspeichern` generiert die Dokumente und speichert sie intern ab. Das Dokument verändert sich nicht mehr, falls zB. das Layout oder Datum ändert.
* *Zeugnis (Grade Report)* und *Studienbestätigung (Enrollment Confirmation)* neu als eigenständiger Doctype.

## Version 1.0

* Erste Version
